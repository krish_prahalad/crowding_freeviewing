from psychopy import core, visual, event, monitors, data, gui
import numpy as np
from psychopy.iohub import (ioHubExperimentRuntime, module_directory)
import os
import time
import random
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
import pixlet
import customTime
from PixelConversion import PixelConversion
from make_mask import make_mask  
#===================================================================================================================================
# We only change the next few lines
subj_id=1
session_num=1
trial_num=1
eccentricity_deg=12
#options:
# 1. Size
# 2. Duration
questParam='duration'
#===================================================================================================================================
clock = psychopy.core.Clock()
# Equipment setup
monsize =  [3840, 2160]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)
window.setMouseVisible(False)
# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=3840
vert_res=2160
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 45 #cm
#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm,cm_per_degree=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=0.5 * pixel_per_degree # o.3 deg or 20 arc min
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=6, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

# We create another window which would be used in the trials with free viewing
fix_rect_width= 25 * pixel_per_degree # 25 degree width
fix_rect_height= fix_rect_width/2
fix_rect=visual.Rect(window,width=fix_rect_width,height=fix_rect_height,units='pix',lineColor='black', fillColorSpace='rgb',pos=(0,0))

#===================================================================================================================================
## Experimental Parameters

#ONLY PART TO BE CHANGED AT THE START OF EXPERIMENT:
#The following set of variables control the main set of parameters for the experiment 
# The following are the list of parameters that can be altered and brief description of what it does to
# the protocol:
# eccentricity_deg: eccentricity of the target in degrees visual angle
# location_uncertainity: This determines whether the stimulus would change in position across the vertical midline b/w trials
# inner_flanker: We would want to remove the inner flanker to prevent overlap with the fixation target at close eccentricities
# cue_condition: This defines whether a cue would be used in the experimental sequence or not
# reverse_flanker_contrast: changes the polarity of the flankers
# foveal_mode: the targets would be presented at the point of fixation
# save_image: To save stimulus frame as image
cue_condition=False

# These parameters are set locally (Stimulus Machine)
num_repeats=1# Sets the number of repeats for each flanker spacing at a given ecc
mask_condition=True
location_uncertainity=False
inner_flanker=True
reverse_flanker_contrast=False
save_image=False 
foveal_mode=False
behav_output=True
# We would draw stimuli based on target type, currently this can be one of the following:
# 1. bars
# 2. E
# 3. Numbers
# 4. Gratings
target_type='grating'

# we would have two types of mask:
#   1. Pixel noise and this would be used when we present gabor stimuli
#   2. jumbled characters of the stimulus that is being presenter. Would be used for all other stimuli
if target_type=='grating':
    mask_type='noise'
else:
    mask_type='jumbled'

# These parameters would be set at the start of the experiment and ideally would not want to change them 
# after the start of the experiment
#Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
# on the fixation cross for variable duration before stimulus onset
# Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
target_presentation=0.096 #sec
cue_presentation= 0.050 #sec
fixation_duration=0.450 #sec 
mask_duration=0.200 #sec
response_window=2.00#sec

# We would also key in the spatial frequency and size of the grating
if target_type=='grating':
    stim_cpd=2 #cycles per degree
    stim_size=1.5
    target_orientation=15

# This creates a list of different flanker spacings to be used which would be multiplied
# with the letter height to given spacings as a function of letter height : nominal spacings
# We also randomize the order of the flanker spacing on each run of the script
# Depending on the spacing condition that is set at the start of the experiment we would run all spacings otherwise
# we would run just the ones specified manually below
flanker_space=[100]
timing_list=np.arange(0.5,0.75,0.050)

#We would then generate multiply the each element of the flanker spacing list based on the number of repeates required at each level
flanker_spacing_list=np.repeat(flanker_space,num_repeats)
random.shuffle(flanker_spacing_list)
aflanker=flanker_spacing_list[0]
#===================================================================================================================================
## Experiment Stimuli: Here we create the different stimuli that would later be used in the experiment

# The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
if location_uncertainity==True:
    stim_location_list=['left','right']
    fix_xpos=0
    msg_offset=200
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line
else:
    stim_location_list=['right'] #This would make all stimuli appear to the right or left of the fixation target
    fix_xpos=0
    msg_offset=200
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line


#Create basic stimuli here that could be called multiple times during the course of the experiment
small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree,text='Press any key to start the experiment')
wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Please wait...')   

# The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
# Cue for rightward target location
cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
#cue for leftward target location
cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)

#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
# Here we would draw it based on the target type that was selected earlier
if target_type=='E' or target_type=='bars':
    target = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_left = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_right = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_up = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
    flanker_down = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
    target.font="Sloan"
    flanker_left.font="Sloan"
    flanker_right.font="Sloan"
    flanker_up.font="Sloan"
    flanker_down.font="Sloan"
elif target_type=='numbers':
    target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
    flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
elif target_type=='grating':
    target = visual.GratingStim(window,pos=(0,0), color=[-1,-1,-1] ,size=stim_size*pixel_per_degree,sf=stim_cpd/pixel_per_degree,mask='gauss')
    flanker_left = visual.GratingStim(window,pos=(0,0), color=[-1,-1,-1] ,size=stim_size*pixel_per_degree,sf=stim_cpd/pixel_per_degree,mask='gauss')
    flanker_right = visual.GratingStim(window,pos=(0,0), color=[-1,-1,-1] ,size=stim_size*pixel_per_degree,sf=stim_cpd/pixel_per_degree,mask='gauss')
    flanker_up = visual.GratingStim(window,pos=(0,0), color=[-1,-1,-1],size=stim_size*pixel_per_degree ,sf=stim_cpd/pixel_per_degree,mask='gauss')
    flanker_down = visual.GratingStim(window,pos=(0,0), color=[-1,-1,-1] ,size=stim_size*pixel_per_degree,sf=stim_cpd/pixel_per_degree,mask='gauss')
# This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
# The letter size is kept constant and is set to 1.5x the threshold from the isolated letter threshold measures.
# To ensure letter size is supra threshold
letter_height=0.5*1.5
letter_height_pix=letter_height*pixel_per_degree

if mask_type=='noise':
    # we create a mask that would cover both the stimulus and the flankers
    # Note: here we use a noise mask over the number mask, to make it a little easier to do the task/ less effect
    # Here the mask is just a random binary image that would change on every trial
    # Mask parameters
    mask_height=250
    mask_width=250
    # Create binary numpy array for each of the masks 
    mask_array_1=np.random.randint(2,size=(mask_height,mask_width))
    mask_target=visual.ImageStim(window,image=mask_array_1,units='pix',size=(mask_width,mask_height),colorSpace='rgb')

    mask_array_2=np.random.randint(2,size=(mask_height,mask_width))
    mask_inner_flanker=visual.ImageStim(window,image=mask_array_2,units='pix',size=(mask_width,mask_height),colorSpace='rgb')

    mask_array_3=np.random.randint(2,size=(mask_height,mask_width))
    mask_outer_flanker=visual.ImageStim(window,image=mask_array_3,units='pix',size=(mask_width,mask_height),colorSpace='rgb')

    mask_array_4=np.random.randint(2,size=(mask_height,mask_width))
    mask_up_flanker=visual.ImageStim(window,image=mask_array_4,units='pix',size=(mask_width,mask_height),colorSpace='rgb')

    mask_array_5=np.random.randint(2,size=(mask_height,mask_width))
    mask_lower_flanker=visual.ImageStim(window,image=mask_array_5,units='pix',size=(mask_width,mask_height),colorSpace='rgb')
elif mask_type=='jumbled':
    # We also create a set of post masks for different letter sizes that might be used in the experiment. 
    # Note: The main advantage of pre-rendered masks is to ensure smooth operation of the script 
    prog_bar = visual.Rect(win=window, pos=(0,0), size=(200,30), lineColor='black',fillColor='black')
    render_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Rendering Masks...')
    target_location_msg=visual.TextStim(window, pos=(0,-msg_offset),color=[-1,-1,-1],height=pixel_per_degree/4,text=("Stimulus eccentricity is %d degrees"%(eccentricity_deg)))
    target_location_msg.draw()
    render_msg.draw()
    window.flip()

    # Mask parameters
    mask_height=250
    mask_width=250
    mask_density=0.4
    num_mask=5 #4 masks for flankers and one for the target

    prog_bar.size=(200*(6)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    # we then create the masks
    mask_target=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(5)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask_inner_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(4)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask_outer_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(3)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask_up_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(2)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask_lower_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(1)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
       
#===================================================================================================================================
## Start of main loop of experiment
# Get a time stamp at the start of the experiment
exptStartTime=customTime.millis()
# SETUP QUEST PARAMETERS
#Draw a fixation cross and wait for the start of the experiment
keepGoing=True
while keepGoing:
    for stim1 in [fix_v, fix_h, fix_c]:
        stim1.draw()

    wait_msg.draw()
    window.flip()
    keys=event.getKeys()

    if len(keys)>0:
        keepGoing=False
# based on Quest Param we keep the parameters constant
if questParam=='size':
    starting_stim_size=1.5
    log_size=np.log10(starting_stim_size)
    minSize=np.log10(0.35)
    staircase= data.QuestHandler(log_size, 0.5, pThreshold=0.75,gamma=0.5, beta=3.5, nTrials=40,
            minVal=minSize) 
elif questParam=='duration':
    starting_duration=0.100
    log_duration=np.log10(starting_duration)
    minDur=np.log10(1/refreshRate) # based on min time between screen flips
    staircase= data.QuestHandler(log_duration, 0.5, pThreshold=0.75,gamma=0.5, beta=3.5, nTrials=40,
            minVal=minDur)
# We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
# accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
# data
if behav_output==True:
    # we create a new variable that would hold the value for flanker spacing 
    # to be used for naming the output file
    #if aflanker is a float this would result in a value !=0 else would be 0
    if int(aflanker) % float(aflanker)==0: 
        flanker_val=int(aflanker)
        flanker_val_str='%d'%(flanker_val)
    else:
        flanker_val=float(aflanker)
        flanker_val_str='%.1f'%(flanker_val)
    if questParam=='size':
        trial_cond="Quest_size"
    elif questParam=='duration':
        trial_cond="Quest_duration"
    outfilename = ("results/S0%s_S%s_T%03d_ecc_%d_flanker_%s_%s_%s.csv" % (subj_id,session_num,trial_num,eccentricity_deg,flanker_val_str,trial_cond,
                time.strftime("%m%d%Y", time.localtime() ) ) )
    threshold_filename = ("results/S0%s_S%s_T%03d_ecc_%d_flanker_%s_%s_%s_CrowdingThresh.csv" % (subj_id,session_num,trial_num,eccentricity_deg,flanker_val_str,trial_cond,
                time.strftime("%m%d%Y", time.localtime() ) ) )
    outfile = open(outfilename, "a")
    threshfile=open(threshold_filename,"a")
    #Add the header row 
    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,% s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc","fix_dur",
                "cue_condition","ISI","stim_dur","RT","corr","stimTime","questCounter") )
    threshfile.write("%s,%s\n"%("flankerSpacing","orientation"))

for repeat in range(num_repeats):
    # This is where the QUEST method begins
    questCounter=0
    #An escape key press here terminates the loop
    if keys[0]!='escape':
        for target_param in staircase:
            if questParam=='size':
                stim_size=10**target_param
            elif questParam=='duration':
                target_presentation=10**target_param
            #This is used to provide a audio feedback to the user 
            beep_sound_correct=Sound(value=1000,secs=0.25)
            beep_sound_incorrect=Sound(value=250,secs=0.25)            
            if target_type=='bars':
                target_let='e'
                target_orientation=[0,270]
            elif target_type=='E':
                target_let='E'
                target_orientation=[0,90,180,270]
            elif target_type=='numbers':
                target_nums=['0','1','2','3','4','5','6','7','8','9']
                random.shuffle(target_nums)
                target_let=target_nums[0]
                flanker_let_1=target_nums[1]
                flanker_let_2=target_nums[2]
                flanker_let_3=target_nums[3]
                flanker_let_4=target_nums[4]
            elif target_type=='grating':
                # we loop till we get non zero random number
                while True:
                    rand_num=random.randint(-1,1)
                    if rand_num!=0:
                        break
                target_orientation=(target_orientation) * rand_num
                # using the same orientation of the target we have flanker orientations
                # to be either clockwise or anti-clockwise. Chosen randomly
                flanker_orientation=[target_orientation,-target_orientation]

            #The letter size is set here
            if target_type!='grating':
                letter_size_deg=letter_height
                letter_size_pix=letter_size_deg*pixel_per_degree
                target.height=letter_size_pix
                flanker_left.height=letter_size_pix
                flanker_right.height=letter_size_pix
                flanker_up.height=letter_size_pix
                flanker_down.height=letter_size_pix
            else:
                target_size_deg=stim_size
                target_size_pix=target_size_deg*pixel_per_degree
                target.size=target_size_pix
                flanker_left.size=target_size_pix
                flanker_right.size=target_size_pix
                flanker_up.size=target_size_pix
                flanker_down.size=target_size_pix
                
            #THe flanker spacing is set on eacn trial based on the stimulus size
            flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
                    
            # Now that we have the stimulus size and flanker spacing we rotate the target letter and 
            # flankers independently 
            # Note: This would only have to be done in the case of tumbling E's and bars 
            if target_type=='E' or target_type=='bars':
                letter_orientation=([(np.random.choice(target_orientation)) for n in [0,1,2,3,4] ])
                target_orientation=letter_orientation[0]
                target.ori=letter_orientation[0]
                flanker_left.ori=letter_orientation[1]
                flanker_right.ori=letter_orientation[2]
                flanker_up.ori=letter_orientation[3]
                flanker_down.ori=letter_orientation[4]
            elif target_type=='grating':
                target.ori=target_orientation
                flanker_left.ori=np.random.choice(flanker_orientation)
                flanker_right.ori=np.random.choice(flanker_orientation)
                flanker_up.ori=np.random.choice(flanker_orientation)
                flanker_down.ori=np.random.choice(flanker_orientation)

            # we then set the respective target and flanker characters
            if target_type!='grating':
                target.text=target_let
                flanker_left.text=flanker_let_1
                flanker_right.text=flanker_let_2
                flanker_up.text=flanker_let_3
                flanker_down.text=flanker_let_4

            # we can also switch the contrast between the target and flankers
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]

            # MOCS: to prevent the same mask from being used on every trial we shuffle the order of masks
            # to switch between the masks used for target and flankers
            masks=[mask_target,mask_inner_flanker,mask_outer_flanker,mask_up_flanker,mask_lower_flanker]
            random.shuffle(masks)
            target_mask=masks[0]
            left_flanker_mask=masks[1]
            right_flanker_mask=masks[2]
            up_flanker_mask=masks[3]
            lower_flanker_mask=masks[4]

            # The following set of code is used to present the stimulus & cue at pre-determined
            # locations and duration
            # The experimental sequence is as follows:
            # 1. Variable delay b/w 750-2000 msec 
            # 2. Stimulus presentation for 48 msec
            # 3. Post-mask for 200 msec
            # 4. Response window following mask offset and reaction time is measured in relation 
            #       to stimulus onset

            # the fixation period before each trial is set to a constant value
            clock.reset()
                    
            #we wait for a pseudo-random time before stimulus onset
            #to prevent anticipation
            ISI=np.random.choice(timing_list)
            numFlips=int(ISI * refreshRate)
            nflips=0
            while nflips<= numFlips:
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw() 
                window.flip()
                nflips+=1
            
            # we position things based on the fixation cross
            fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 

            # The following set of lines set the flanker condition 
            # Additionally the horizontal position of the target is randomized
            eccentricity_pix=eccentricity_deg*pixel_per_degree
            stim_location=np.random.choice(stim_location_list)
            # The following set of lines set the flanker condition 
            eccentricity_pix=eccentricity_deg*pixel_per_degree
            if stim_location=="left":
                eccentricity_x=fixation_pos[0]-eccentricity_pix
            elif stim_location=="right":
                eccentricity_x=fixation_pos[0]+eccentricity_pix
            eccentricity_y=fixation_pos[1]
            if foveal_mode==True:
                target_location=(fix_h.start[0],0)
            else:
                target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 
            #We also have a condition where the target can be presented at the foveally
            # replacing the fixation cross
            if foveal_mode==True:
                target_location=(fix_h.start[0],0)
            else:
                target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 
            # we use this to center the target and flanker positions
            target.pos=target_location
            flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1])
            flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1])
            flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix)
            flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix)
            #we center the mask with the stimulus
            target_mask.pos=target_location 
            left_flanker_mask.pos=flanker_left.pos
            right_flanker_mask.pos=flanker_right.pos
            up_flanker_mask.pos=flanker_up.pos
            lower_flanker_mask.pos=flanker_down.pos
            # The stimulus duration is used as a fraction of the frame rate of the display;
            # THis ensures better timing of the target presentations
            #we also the reset the kb clock to the reaction time in relation to stimulus onset
            startTime = customTime.millis()
            # we clear key events that could occured after the end of the previous trial 
            event.clearEvents()
            if target_type=='numbers':
                target.render()
                flanker_right.render()
                flanker_left.render()
                flanker_up.render()
                flanker_down.render()

            for frameN in range(int(refreshRate*target_presentation)):
                # We only draw the fixation cross for the peripheral condition and not for the foveal one
                # Note: we do not move the location of the fixation cross
                if foveal_mode==False:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                # we test whether the stimulus would appear on the left/right and based
                # on that we set the inner and outer flankers
                if stim_location=='left':                    
                    inner_flanker=flanker_right
                    outer_flanker=flanker_left
                elif stim_location=='right':
                    inner_flanker=flanker_left
                    outer_flanker=flanker_right
                # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
                if inner_flanker==True or foveal_mode==True:
                    inner_flanker.draw()
                if target_type=='numbers':
                    target.render()
                    flanker_right.render()
                    flanker_left.render()
                    flanker_up.render()
                    flanker_down.render()
                target.draw()                
                outer_flanker.draw()
                flanker_up.draw()
                flanker_down.draw()
                # we get the time stamp for when the first frame of the stimulus comes on
                if frameN==0:
                    stimTime=(customTime.millis()-exptStartTime)/1000
                # we capture the stimulus on the buffer before adding in the square marker
                # This can later be saved if needed
                if save_image==True:# & frameN==0:
                    window.getMovieFrame(buffer='back')
                flip_time=window.flip()
            # Followin the stimulus offset we have a post-mask presented immediately after the same
            if mask_condition==True:
                #we center the mask with the stimulus (using the most recent location)
                target_mask.pos=target_location 
                left_flanker_mask.pos=flanker_left.pos
                right_flanker_mask.pos=flanker_right.pos
                up_flanker_mask.pos=flanker_up.pos
                lower_flanker_mask.pos=flanker_down.pos
                for frameN in range(int(refreshRate*mask_duration)):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    # we would set the inner and outer flanker masks based on the position of the target/stimulus
                    if stim_location=='left':
                        inner_flanker_mask=right_flanker_mask
                        outer_flanker_mask=left_flanker_mask
                    elif stim_location=='right':
                        inner_flanker_mask=left_flanker_mask
                        outer_flanker_mask=right_flanker_mask
                    # We would draw the inner flanker mask only when the inner flanker is also used otherwise we would skip this
                    if inner_flanker==True or foveal_mode==True:
                        inner_flanker_mask.draw()
                    # we would draw separate masks for target and flankers
                    target_mask.draw()
                    outer_flanker_mask.draw()
                    up_flanker_mask.draw()
                    lower_flanker_mask.draw()
                    # we capture the mask buffer and save to image file when needed
                    if save_image==True:# & frameN==0:
                        window.getMovieFrame(buffer='back')

                    window.flip()
            #Use this to save the stimulus image
            if save_image==True:
                window.saveMovieFrames('Stimulus_'+str(np.abs(eccentricity_deg))+'_'+str(flanker_deg)+'_'+str(stim_location)+'.jpg')

                    
            # we wait for the behavioral response from the subject. The waiting duration is set at the start of the experiment
            # The duration is mainly to ensure that the timing of each trial is pre-determined and can be used to estimate the overall
            # duration of the corresponding retinal video recording on the TSLO machine
            reaction_timer = core.CountdownTimer(response_window)
            keepGoing=True
            while reaction_timer.getTime()>0 and keepGoing==True:
                response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2)
                response_wait.draw()
                
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                    
                flip_time=window.flip()

                key=event.getKeys()
                if len(key)>0:
                    response_time=customTime.millis()
                    keepGoing=False
                else:
                    response_time=customTime.millis()
                            
            # we check if there was a keypress made duration the response window otherwise we set a defualt value
            if 'key' not in locals():
                key_pressed='Nil' # arbitary value 
            elif len(key)==0:
                key_pressed='Nil'
            else:
                key_pressed=key[0]

            if key_pressed=='escape':
                break
                                            
            #we also store the reaction time
            RT=(response_time - startTime)/1000
            # For the response keys we have separate conditions for when using tumbling e, 3 bar and numbers conditions
            if target_type=='E' or target_type=='bars':
                if key_pressed=='num_6' or key_pressed=='right' :
                    response_dir=0
                elif key_pressed=='num_2' or key_pressed=='down':
                    response_dir=90
                elif key_pressed=='num_4' or key_pressed=='left':
                    response_dir=180
                elif key_pressed=='num_8' or key_pressed=='up':
                    response_dir=270
                else:
                    response_dir=45 #arbitary value to prevent the program from crashing
            elif target_type=='bars':
                if key_pressed=='right' or key_pressed=='left' or key_pressed=='num_6' or key_pressed=='num_4':
                    response_dir=0
                elif key_pressed=='down' or key_pressed=='up' or key_pressed=='num_2' or key_pressed=='num_8':
                    response_dir=270
                else:
                    response_dir=45 #arbitary value to prevent the program from crashing
            elif target_type=='numbers':
                if key_pressed=='num_0':
                    response_num='0'
                elif key_pressed=='num_1':
                    response_num='1'
                elif key_pressed=='num_2':
                    response_num='2'
                elif key_pressed=='num_3':
                    response_num='3'
                elif key_pressed=='num_4':
                    response_num='4'
                elif key_pressed=='num_5':
                    response_num='5'
                elif key_pressed=='num_6':
                    response_num='6'
                elif key_pressed=='num_7':
                    response_num='7'
                elif key_pressed=='num_8':
                    response_num='8'
                elif key_pressed=='num_9':
                    response_num='9'
                else:
                    response_num='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
            elif target_type=='grating':
                if key_pressed=='right'or key_pressed=='num_6' :
                    response_dir=1
                elif key_pressed=='left' or key_pressed=='num_4':
                    response_dir=-1
                else:
                    response_dir=0

            if target_type=='E' or target_type=='bars' or target_type=='grating':
                if target_type=='grating':
                    if target.ori>0: # positive rotations
                        target_dir=1
                    else: # negative rotations
                        target_dir=-1
                else:
                    target_dir=target.ori

                if response_dir==target_dir:
                    beep_sound_correct.play()
                    corr=1
                else:
                    corr=0
                    beep_sound_incorrect.play()
            elif target_type=='numbers':
                if response_num==target_let:
                    beep_sound_correct.play()
                    corr=1
                else:
                    beep_sound_incorrect.play()
                    corr=0
            # we write the responses to the output file     
            if key_pressed!='escape' and behav_output==True:
                if target_type=='E' or target_type=='bars':
                    outfile.write("%s,%.2f,%d,%d,%d,%d,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%2f,%d\n"%(response_dir,target.ori,inner_flanker.ori,outer_flanker.ori,flanker_up.ori,flanker_down.ori,
                    letter_size_deg,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT,corr,stimTime,questCounter) )
                elif target_type=='grating':
                    outfile.write("%s,%.2f,%d,%d,%d,%d,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%2f,%d\n"%(response_dir,target.ori,inner_flanker.ori,outer_flanker.ori,flanker_up.ori,flanker_down.ori,
                    stim_size,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT,corr,stimTime,questCounter) )
                elif target_type=='numbers':
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%2f,%d\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                    letter_size_deg,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT,corr,stimTime,questCounter) )
            
            # inform QUEST of the response, needed to calculate the next level
            staircase.addResponse(corr)
            questCounter+=1

# We get the threshold from the quest method
s=10**(staircase.mean())
if behav_output==True:
    threshfile.write("%.2f,%.2f\n"%(aflanker,s))
    threshfile.close()
    print('The threshold is %2f'%(s))


# We would print out a message for the subject and wait for them to hit a key before restarting the program
loop_msg1=visual.TextStim(window,text='Press any key to continue!',pos=(0,msg_offset),height=pixel_per_degree)
loop_msg1.draw()
window.flip()
event.waitKeys()

window.close()
