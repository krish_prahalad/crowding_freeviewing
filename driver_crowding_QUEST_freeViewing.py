from Crowding_FreeViewing_Quest import ExperimentRuntime
from psychopy.iohub import (ioHubExperimentRuntime, module_directory)
import numpy as np
from psychopy import gui
import random
import os

#Create a gui where the examiner enters the subject info and expt parameters
myDlg = gui.Dlg(title="Crowding Experiment")
myDlg.addText('Subject Information')
myDlg.addField('Subject ID:',1)
myDlg.addField('Session Number:',2)
myDlg.addField('Trial Number:',1)
myDlg.addField('Testing Eye:', choices=["right", "left"])
myDlg.addText('Experiment Info:')
myDlg.addField('Stimulus Type:', choices=["grating", "numbers"])
myDlg.addField('Target Eccentricity:',12)
myDlg.addField('Flanker Spacings:',choices=["All","unflanked only"])
myDlg.addField('Viewing Condition:',choices=["Both","Free Viewing only", "Fixation only"])
myDlg.addField('Location uncertainity:',choices=["False","True"])
ok_data = myDlg.show()  # show dialog and wait for OK or Cancel
if myDlg.OK:  # or if ok_data is not None
    # we set the parameters based on user input
    # Subject info
    subj_id=ok_data[0]
    session_num=ok_data[1]
    trial_num=ok_data[2]
    testing_eye=ok_data[3]
    # Stimulus parameters
    target_type=ok_data[4]
    target_eccentricity=ok_data[5]
    # flanker spacings
    if ok_data[6]=="All":
        flanker_spacing_list=[1.6,2.3,3,100]
    else:
        flanker_spacing_list=[100]
    #Viewing condition
    if ok_data[7]=="Both":
        viewing_conds=[1,0]
        random.shuffle(viewing_conds)
    elif ok_data[7]=="Free Viewing only":
        viewing_conds=[1]
    else:
        viewing_conds=[0]
    # Target location uncertainity
    if ok_data[8]=="True":
        location_uncertainity=True
    else:
        location_uncertainity=False
else:
    print('user cancelled')

# Check if trial counter is unique otherwise increment by one
try:
    trialCode='S0%d_S%d_T00%d'%(subj_id,session_num,trial_num)
    data_dir="results"
    # Keep looping till we get unique trialCode
    # Just to prevent any overwrites
    uniqueFile=False
    while uniqueFile==False:
        for afile in os.listdir(os.path.join(os.getcwd(),data_dir)):
            if trialCode in afile:
                trial_num+=1
                # new trial code
                trialCode='S0%d_S%d_T00%d'%(subj_id,session_num,trial_num)
                break
            else: # file is unique
                uniqueFile=True
                break
    for aviewing_cond in viewing_conds:
        # initiate counter
        flanker_counter=1
        #randomize flankers
        random.shuffle(flanker_spacing_list)
        for aflanker in flanker_spacing_list:
            # We would change the iohub config based on the eye being tested
            # We would read in the config file first
            with open('iohub_config.yaml','r') as file:
                lines=file.readlines()
            # Tweak the line that contains testing eye info
            if testing_eye=='right':
                lines[36]=lines[36][0:lines[36].find(":")+1]+' RIGHT_EYE\n'
            elif testing_eye=='left':
                lines[36]=lines[36][0:lines[36].find(":")+1]+' LEFT_EYE\n'
            # Write the info back out into iohub config file
            with open('iohub_config.yaml','w') as file:
                file.writelines(lines)
            runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
            # Set subject parameters
            runtime.subj_id=subj_id
            runtime.session_num=session_num
            runtime.trial_num=trial_num
            runtime.testing_eye=testing_eye
            #Experiment parameters
            runtime.condition_num=flanker_counter/len(flanker_spacing_list)
            runtime.target_type=target_type
            runtime.eccentricity_deg=target_eccentricity
            runtime.flanker_spacing=aflanker
            runtime.free_viewing_condition=aviewing_cond
            runtime.num_repeats=1
            runtime.location_uncertainity=location_uncertainity
            runtime.mask_condition=True
            runtime.behav_output=True
            runtime.inner_flanker_condition=True
            runtime.target_rotation='both'
            runtime.mask_condition=True
            runtime.flanker_rotation='vertical'
            runtime.threshold_method='staircase'
            # Start experiment
            runtime.start()
            # increment counter
            flanker_counter+=1
except NameError:
    pass