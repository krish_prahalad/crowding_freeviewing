from psychopy import data, visual, core, event
import random # os  datetime, itertools, csv
import numpy as np
import pixlet
# This function will generate a bunch of random, randomly-oriented letters,
# suitable for a mask. It uses PsychoPy to directly draw the letters in a buffer (just as if shown in an expt.)
# Then it extracts the image, which should be fed to an image stimulus you draw.
# Drawing the pre-computed image is faster than re-generating the random letters each frame.

# If you specify a single color, that will be used for all letters
# If you specify multiple colors, each letter's color will be randomly chosen from this list

# fontnames tested: 'Arial', 'Courier', 'Times'

# It creates a grid to fill up the buffer size provided.
# Density of 1.0 makes the letters pretty much touching horizontally. Thus they'll overlap a bit when you rotate.
# 1.0 seems pretty good. More makes more closely spaced /denser, less spaces letters farther apart

def make_mask(win, letter_height, mask_size=(100,100),colors=[-1,-1,-1], fontname='Courier New',density=1.0 ,tumbling_e=True,numbers=True):

    # Erase what is already on screen:
    win.clearBuffer()

    # Make a grid with the following x,y spacing:
    row_spacing=letter_height*0.46/density
    col_spacing=letter_height*0.46/density
    # Need enough rows,cols to cover window. (Add some extra, since we'll padd to start letters halfway in)
    ncols=(int( mask_size[0]/col_spacing + 2  ))
    nrows=(mask_size[1]//row_spacing+1)
    nlets=(nrows)*(ncols)
    fudge=(letter_height//4,letter_height//2) #add a little bit so that letters have a bit of space
    grid_start_offset=(-win.size[0]//2+fudge[0],win.size[1]//2-fudge[1]) #start in uppter left hand corner

    # Make a bunch of letter widgets. (Will change individual properties later.)
    if numbers==True:
        lets=[pixlet.PixLet(win=win,pos=(0,0),alignHoriz='center', alignVert='center',height=letter_height,units='pix') for alet in np.arange(nlets-1)]
    else:
        lets=[visual.TextStim(win,font=fontname, alignHoriz='center',alignVert='center',height=letter_height, units="pix") for alet in np.arange(nlets-1)]

    # Precompute random orientations
    oris=np.random.randint(-135,-45,size=len(lets))

    # Then randomize the text, position, and rotation of those widgets, also draw each:
    # Position will be a grid starting at (0,0)
    for nlet,alet in enumerate(lets):
        if np.shape(colors)==(3,):
            color1=colors
        else: # If multiple colors specified, randomly pick a color for each
            color1=random.choice(colors)
        alet.color=color1
        # Note: We currently have it set up to work with random letters/tumbling E/ bars/ numbers
        if numbers==True:
            alet.text=str(np.random.randint(0,9))
        else:
            if tumbling_e==True:
                alet.text='e'
            else:
                alet.text=chr(ord('e')+np.random.randint(0,26)) 
        alet.ori=oris[nlet]
        alet.pos=((nlet%ncols)*row_spacing+grid_start_offset[0],-(nlet//ncols)*col_spacing+grid_start_offset[1]) 
        if numbers==True:
            alet.render()
        alet.draw()

    # Save the image of the scrambled letters. (Into a PIL image)
    # Crop out the very middle of it and return that.
    scrambled_letters=visual.BufferImageStim(win).image
    img_width,img_height=scrambled_letters.size
    # For PsychoPy, drawing starts at (0,0) (the middle of the screen)
    # Need to figure out actualy coords for PIL/PIllow's crop function, which goes:
    # crop( left, top, right, bottom )
    bits=scrambled_letters.crop((0,0,mask_size[0],mask_size[1]))

    # It seems the color is just a multiplier, so should keep it 1
    mask_widget = visual.ImageStim(win=win, color=1.0, size=mask_size, units='pix')
    mask_widget.image=bits

    # Clean up after ourselves (erase screen)
    win.clearBuffer(),
    
    return mask_widget

if __name__=="__main__":
    window = visual.Window(fullscr=True, allowGUI=False, colorSpace='rgb', units='pix',screen=1)

    # Generate a random mask
    mask=make_mask(win=window,mask_size=(300,300),letter_height=160 ,density=0.4,tumbling_e=True,numbers=True) #, colors=[(1,0,0),(0,0,1),(1,1,1)] )
    # Alternating colors, patriotic version
#    mask=make_mask(win=window,mask_size=(600,400),letter_height=82,fontname='Arial',density=1.0, 
#            colors=[(1,0,0),(0,0,1),(1,1,1)] )

    mask.draw()
    window.flip()
    keys = event.waitKeys(keyList=['space', 'escape']) 

    window.close()


