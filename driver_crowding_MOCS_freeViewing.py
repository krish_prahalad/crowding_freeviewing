from Crowding_FreeViewing_MOCS import ExperimentRuntime
from psychopy.iohub import (ioHubExperimentRuntime, module_directory)
import numpy as np
from psychopy import gui
import random
import os

#Create a gui where the examiner enters the subject info and expt parameters
myDlg = gui.Dlg(title="Crowding Experiment")
myDlg.addText('Subject Information')
myDlg.addField('Subject ID:',1)
myDlg.addField('Session Number:',1)
myDlg.addField('Trial Number:',1)
myDlg.addField('Testing Eye:', choices=["right", "left"])
myDlg.addText('Experiment Info:')
myDlg.addField('Stimulus Type:', choices=["grating", "numbers"])
myDlg.addField('Target Eccentricity:',8)
myDlg.addField('Viewing Condition:',choices=["Both","Free Viewing only", "Fixation only"])
myDlg.addField('Location uncertainity:',choices=["False","True"])
myDlg.addField('Orientation Threshold:',12)
ok_data = myDlg.show()  # show dialog and wait for OK or Cancel
if myDlg.OK:  # or if ok_data is not None
    # we set the parameters based on user input
    # Subject info
    subj_id=ok_data[0]
    session_num=ok_data[1]
    trial_num=ok_data[2]
    testing_eye=ok_data[3]
    # Stimulus parameters
    target_type=ok_data[4]
    target_eccentricity=ok_data[5]
    #Viewing condition
    if ok_data[6]=="Both":
        viewing_conds=[1,0]
        random.shuffle(viewing_conds)
    elif ok_data[6]=="Free Viewing only":
        viewing_conds=[1]
    else:
        viewing_conds=[0]
    # Target location uncertainity
    if ok_data[7]=="True":
        location_uncertainity=True
    else:
        location_uncertainity=False
    # Orientation threshold
    target_rotation=ok_data[8]
else:
    print('user cancelled')

# Check if trial counter is unique otherwise increment by one
try:
    trialCode='S0%d_S%d_T00%d'%(subj_id,session_num,trial_num)
    data_dir="results"
        # Keep looping till we get unique trialCode
        # Just to prevent any overwrites
    uniqueFile=False
    while uniqueFile==False:
        for afile in os.listdir(os.path.join(os.getcwd(),data_dir)):
            if trialCode in afile:
                trial_num+=1
                # new trial code
                trialCode='S0%d_S%d_T00%d'%(subj_id,session_num,trial_num)
                break
            else: # file is unique
                uniqueFile=True
                break
    # We would change the iohub config based on the eye being tested
    # We would read in the config file first
    with open('iohub_config.yaml','r') as file:
        lines=file.readlines()
    # Tweak the line that contains testing eye info
    if testing_eye=='right':
        lines[36]=lines[36][0:lines[36].find(":")+1]+' RIGHT_EYE\n'
    elif testing_eye=='left':
        lines[36]=lines[36][0:lines[36].find(":")+1]+' LEFT_EYE\n'
    # Write the info back out into iohub config file
    with open('iohub_config.yaml','w') as file:
        file.writelines(lines)
    for aviewing_cond in viewing_conds:
        runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
        # Set subject parameters
        runtime.subj_id=subj_id
        runtime.session_num=session_num
        runtime.trial_num=trial_num
        runtime.testing_eye=testing_eye
        #Experiment parameters
        runtime.target_type=target_type
        runtime.eccentricity_deg=target_eccentricity
        runtime.free_viewing_condition=aviewing_cond
        runtime.num_repeats=10
        runtime.location_uncertainity=location_uncertainity
        runtime.mask_condition=True
        runtime.behav_output=True
        runtime.target_rotation=target_rotation
        runtime.mask_condition=True
        # Start experiment
        runtime.start()
except NameError:
    pass